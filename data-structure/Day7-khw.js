//===========================================현우===============================================================
// I HashTable
// I-1 HashTable class
class HashTable 
{
    constructor()
    {
        this.table = []; // 해시 테이블 배열
        this.tableaddr = []; // 해시 테이블의 주소 값을 저장해둔 배열
    }

    Hashcode(key)
    { // 해시 주소값으로 변환 해주는 만드는 함수
        var hash = 5381;
        for (var i=0; i<key.length; i++)
        {
            hash = hash * 33 + key.charCodeAt(i); //key.charCodeAt(i) : key 값에 해당하는 문자에서 i번째 한 글자의 unicode 값을 리턴
        }
        var result = hash % 1013;  
        this.tableaddr.push(result);
        return result;

    }
    put(key,value)
    {// 해시 테이블에 값을 넣는다. (해쉬 주소값 : value)
        var Hashadrr = this.Hashcode(key); // 해시 주소 값으로 변환
        this.table[Hashadrr] = value;
        console.log("해시 테이블에 추가 -> 해시 주소값:",Hashadrr, "\t\tValue:",value);
    }
    get(key)
    {// 해시 주소 값을 사용하여 해시 테이블에서 특정 값을 꺼낸다.
        return this.table[this.Hashcode(key)];
    }
    remove(key)
    {// 해시 주소 값을 사용하여 해시 테이블에서 특정 값을 지운다.
        if (this.table[this.Hashcode(key)]==undefined) // 해시 테이블에 value 값이 있을 시
        {
                console.log("저장되어 있는 값이 아닙니다.")
        }
        else 
        {
            console.log("해시 테이블에서 삭제 -> 삭제 해시 주소값:",this.Hashcode(key), "\t\tValue:",this.table[this.Hashcode(key)]);
            this.table[this.Hashcode(key)]==undefined; // 해시 주소 값에 해당하는 value 값을 undefined로 바꾼다. (삭제)
        }

    }
    
    toString()
    {
        for (var addr of this.tableaddr) // 해시 주소 값들이 저장되어 잇는 배열의 원소들을 이용하여 해시 주소 값 및 value값을 문장 형태로 출력한다.
        {
            console.log("table 주소값:"+addr+"\t\t Value:"+this.table[addr]);
        }

        /* 다른 방법
        Object.entries(this.table).forEach(([k,v]) => { 
        // forEach() 함수를 이용하여 해시 테이블의 해시 주소 값 및 value 값을 출력한다.
        // ※ forEach() 함수를 이용하면 값이 undefined 인곳은 출력되지 않는다.
            console.log("table 주소값:"+k+"\t\t Value:"+v);
        });
        */
    }
    

}

// I-2 HashTable 과 Dictionary의 공통점과 차이점
/*
공통점 : key 와 value를 이용해 값을 저장한다.
차이점 : 
HashTable는 특정 자료형 뿐만 아니라 여러 자료형을 담을 수 있다 (key 값을 해시 주소값으로 변환하여 해시 테이블이라는 배열의 원소로 저장)
Dictionary는 선언한 자료형에 맞춰서 데이타를 삽입해야 한다
*/



// II HashTable Basic2
// II-1
const hash = new HashTable;
console.log("II-1")
hash.put('Ygritte', 'ygritte@email.com');
hash.put('Jonathan', 'jonathan@email.com');
hash.put('Jamie', 'jamie@email.com');
hash.put('Jack', 'jack@email.com');
hash.put('Jasmine', 'jasmine@email.com');
hash.put('Jake', 'jake@email.com');
hash.put('Nathan', 'nathan@email.com');
hash.put('Athelstan', 'athelstan@email.com');
hash.put('Sue', 'sue@email.com');
hash.put('Aethelwulf', 'aethelwulf@email.com');
hash.put('Sargeras', 'sargeras@email.com'); 
console.log();

// II-2
console.log("II-2");
for (var elem of hash.tableaddr) // 해쉬 주소 값을 출력
{
    console.log("hash table 주소: ", elem);
}
// II-3
/* HashTable 클래스에 tostring() 함수로 구현 */

// II-4
console.log()
console.log("II-4")
hash.toString();
//===========================================현우===============================================================