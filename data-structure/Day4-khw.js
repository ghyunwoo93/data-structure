class ListNode {
    constructor(element, next = null) {
        this.element = element;
        this.next = next;
    }
}
class LinkedList {
    constructor() {
        this.head = null;
        this.length = 0;
    }

    append(element) 
    {
        var node = new ListNode(element);
        var current = this.head;
        if (this.head==null) 
        { 
            this.head = node;
        } 
        else 
        {
            while (current.next) 
            {
                current = current.next;
                
            }
            current.next = node; 
        }
        this.length++;
        return this.head;
    }

    insert (position, element)
    {
        if (position >= 0 && position <= this.length)
        {
            var node = new ListNode(element);
            var current = this.head;
            var previous;
            var index = 0;
            if (position == 0)
            {
                node.next = current; 
                this.head = node;
            } 
            else 
            {
                while (index++ < position)
                { 
                    previous = current;
                    current = current.next;
                }
                node.next = current;
                previous.next = node;
            }
            this.length++; 

            return true;
        } 
    
        else 
        {
            return false;
        }
   }
   print() 
   { 
        var lista = []; 
        if (this.head==null) 
        {
            console.log("리스트가 비였습니다.");
        } 
        else 
        { 
            var node = this.head; 
            while (node) 
            { 
                lista.push(node.element); 
                node = node.next; 
            } 
            console.log(lista); 
        } 
    }

    size() 
   {
        return this.length;
   }

}
/*
// 2번 출력
var list = new LinkedList;
list.append(4);
list.append(7);
list.append(8);
list.append(10);
console.log(list);
list.print();
*/

// 3번 출력
var list2 = new LinkedList;
list2.append(1);
list2.append(3);
list2.append(4);
console.log(list2)
list2.insert(1, 2);
console.log(list2)
list2.print();

