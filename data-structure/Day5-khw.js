
///////////////////////////////////////////////////////////////////////현우
// I. Sets General
/*
 var Test = new Set();

//I-1 add(),delete()
// add() 메서드는 새로운 요소가 추가된 Set 객체를 반환한다.
console.log("add")
Test.add(1);
Test.add(2);
Test.add("a");
console.log(Test,'\n\n'); // -> Set(3) { 1, 2, 'a' } 

// delete() 메서드는 Set 객체의 특정 요소를 삭제한다.
// ※ set 객체는 인덱스를 갖기 않기 때문에 delete() 메서드에는 인덱스가 아니라 삭제하려는 요소값을 인수로 전달해야한다.
// ※ 삭제 성공여부를 나타내는 불린 값을 반환한다.

console.log("delete");
console.log(Test.delete("a")); // 삭제 성공 -> 불린 값 반환 -> true
console.log(Test,'\n\n'); // Set(2) { 1, 2 } 


// I-2, has(), clear(), size(), values()
// has() 메서드는 특정 요소가 Set 객체에 있는지 없는지를 확인하여 불린 값을 반환한다.
console.log("has");
console.log(Test.has(2)); // 있으므로  ture
console.log(Test.has("b"),'\n\n'); // 없으므로  false

// clear() 메서드는 Set 객체의 요소들을 일괄 삭제한다.
console.log("clear");
console.log(Test.clear(),'\n\n');  // -> undefined

// size() 메서드는 Set 객체의 요소 개수를 반환한다.
console.log("size");
Test.add("c")
console.log(Test.size,'\n\n'); // -> 1

// values() 메서드는 Set 객체에 요소가 삽입된 순서대로 각 요소의 값을 반환한다.
var setIter = Test.values();
Test.add("d");
console.log("values");
console.log(setIter.next().value);  // -> c
console.log(setIter.next().value,'\n\n'); // -> d
*/




//I-3 Union, Interserction, Differences, Subset
//Union : 두 Set 객체의 합집합을 반환하는 함수이다.
/*
Set.prototype.Union = function(union) {
    for (var elem of union) // 하나의 Set 객체의 요소들을 차례로 뽑아서 다른 객체에 추가 시킨다. 중복 X
    {
        this.add(elem);
    }
    return this;
}

var setUnionA = new Set([1,2,4,7,8,9]);
var setUnionB = new Set([3,4,5]);
console.log(setUnionA.Union(setUnionB));  // -> Set(8) { 1, 2, 4, 7, 8, 9, 3, 5 }


//Intersection : 두 Set 객체의 교집합을 반환하는 함수이다.
Set.prototype.Intersection = function(intersection){
    var result = new Set(); // 교집합을 담을 Set 객체
    for (var elem of intersection) // 하나의 Set 객체의 요소들을 차례 뽑아서 다른 객체에 있는지 확인한다. 존재한다면 위에서 새로 정의한 Set 객체에 넣는다.
    {
        if (this.has(elem))
        {
            result.add(elem);
        }
    }
    return result;
}

var intersectionA = new Set([1,2,3,4,5]);
var intersectionB = new Set([1,3,5]);
console.log(intersectionA.Intersection(intersectionB)); // -> Set(3) { 1, 3, 5 }


//Difference : 두 Set 객체의 차집합을 반환하는 함수이다.
Set.prototype.Difference = function(difference) {
    for (var elem of difference) // 하나의 Set 객체의 요소들을 뽑아서 다른 객체에 있는지 확인한다. 있다면 그 객체를 뺀다.
    {
        if (this.has(elem))
        {
            this.delete(elem);
        }
    }
    return this;
}

var differenceA = new Set([1,2,3,7,8]);
var differenceB = new Set([1,2,3,4,5]);

console.log("A-B :",differenceA.Difference(differenceB)); // -> A-B : Set(2) { 7, 8 }
differenceA.add(1);
differenceA.add(2);
differenceA.add(3);
console.log("B-A :",differenceB.Difference(differenceA)); // => B-A : Set(2) { 4, 5 }


// Subset : 두 Set 객체의 부분 집합을 반환하는 함수이다.
Set.prototype.Subset = function (subset) 
{
    for (var elem of subset ) // 파라미터로 받은 Set의 요소들을 하나씩 뽑아낸다.
    {
        if (this.has(elem)==false) // 기존의 Set에 파라미터로 받은 Set의 요소가 하나라도 없으면 부분 집합이 아니므로 false 반환
        {
            return false; 
        }
    }
    return true; // 모든 요소가 기존의 Set에 들어 있으면 부분 집합이므로 true 반환
}

var TestSetA = new Set([1,2,3,4,5,6,7]); 
var TestSetB = new Set([1,2,3,4,5]); 

console.log("TestSetB 는 TestSetA의 부분 집합 입니까?(True/False): ",TestSetA.Subset(TestSetB)); // -> TestSetB 는 TestSetA의 부분 집합 입니까?(True/False):  true
console.log("TestSetB 는 TestSetA의 부분 집합 입니까?(True/False): ",TestSetB.Subset(TestSetA)); // -> TestSetB 는 TestSetA의 부분 집합 입니까?(True/False):  false
*/



// II. Sets Basic1 
/*
function sizeLegacy(items){ // property를 수동으로 추출하고 그 property의 수를 계산하여 반환하는 함수
    var count = 0;
    var setIter2 = items.values(); 
    items2=setIter2.next().value
    // {{"a":1,"b":2,"c":3,"d":4,"e":5,"f":6,"f":8,"g":9}} 이러한 형태로 Set 및 items가 되어있다. 있다. 안쪽 대괄호 안의 원소들을 사용하기 위해서 
    // value() 메서드를 사용하였다. 67~68 코드로 인해 안쪽 대괄호 안의 원소 들을 사용할 수 있게 되었다.
    for(let key in items2) {  // 키 값을 뽑아낸다.
    if(items2.hasOwnProperty(key))  // 키 값이 items2에 있는지 확인하고 있으면 count를 증가시킨다.
    {
        ++count; 
    }
    }
    return count;
   };

var arr= {"a":1,"b":2,"c":3,"d":4,"e":5,"f":6,"g":7}; // 입력 값 hasOwnProperty를 사용하기 위햐서 key:value 값으로 입력
var set = new Set();
set.add(arr);  // Set에 요소 추가.
console.log("Set Basic 2 출력 값 :",sizeLegacy(set),'\n\n'); // -> Set Basic 2 출력 값 : 7 
*/




// III Sets Basic 3
/*
function valueLegacy(items){ // property를 수동으로 iteration하고 array에 추가 한 후, 그 배열을 반환하는 함수
    var arrvalue = []; // 객체의 값을 담을 배열
    var setIter2 = items.values(); 
    items2=setIter2.next().value
    // {{"a":1,"b":2,"c":3,"d":4,"e":5,"f":6,"f":8,"g":9}} 이러한 형태로 Set 및 items가 되어있다. 있다. 안쪽 대괄호 안의 원소들을 사용하기 위해서 
    // value() 메서드를 사용하였다. 89~90 코드로 인해 안쪽 대괄호 안의 원소 들을 사용할 수 있게 되었다.
    for(let key in items2) {  // 키 값을 뽑아낸다.
    if(items2.hasOwnProperty(key))  // 키 값이 items2에 있는지 확인하고 있으면 키 값에 해당하는 값을 위에서 선언한 배열에 넣는다.
    {
        arrvalue.push(items2[key]);
    }
    }
    return arrvalue.toString(); // 배열 형태를 문자열 형태로 바꾼다.
   };

var arr= {"a":1,"b":2,"c":3,"d":4,"e":5,"f":6,"g":7}; // 입력 값 hasOwnProperty를 사용하기 위햐서 key:value 값으로 입력했다.
var set = new Set();
set.add(arr);  // Set에 요소 추가.
console.log("Set Basic 3 출력 값 :",valueLegacy(set),'\n\n'); // -> Set Basic 3 출력 값 : 1,2,3,4,5,6,7 
*/



//IV Set Advanced 1
/*
Set.prototype.Union2 = function(union) {
    for (var elem of union) // 하나의 Set 객체의 요소들을 차례로 뽑아서 다른 객체에 추가 시킨다. 중복 X
    {
        this.add(elem);
    }
    return this;
}

var setUnionA = new Set([1,2,4,7,8,9]);
var setUnionB = new Set([3,4,5]);

console.log(setUnionA.Union2(setUnionB));  // -> Set(8) { 1, 2, 4, 7, 8, 9, 3, 5 }
var SetA = new Set([1,2,3]);
var SetB = new Set([3,4,5]);

console.log(SetA.Union2(SetB)); // -> Set(5) { 1, 2, 3, 4, 5 }
*/



//V Set Advanced 2
/*
Set.prototype.Intersection2 = function(intersection){
    var result = new Set(); // 교집합을 담을 Set 객체
    var setIter = intersection.values(); 
    for (i=0; i<intersection.size; i++) // Set 객체의 요소가 전부 뽑아질 때 까지 반복
    {
        elem= setIter.next().value // Set 객체의 요소를 하나씩 뽑아서 elem 변수에 넣는다..
        if (this.has(elem)) // 하나의 Set 객체의 요소가 다른 Set 객체의 요소로 존재한다면 위에서 만든 result Set 객체에 추가한다.
        {
            result.add(elem);
        }
    }
    return result;
}

var SetA = new Set([1,2,3,4,5,6,7]);
var SetB = new Set([2,3,4,5]);
console.log(SetA.Intersection2(SetB));// -> Set(4) {2, 3, 4, 5 }
*/

///////////////////////////////////////////////////////////////////////// 현우