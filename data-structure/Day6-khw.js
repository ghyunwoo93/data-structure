//================================현우=======================================================
// I. Dictionary Basic
// I-2
class Dictionary
{
    constructor(){
        this.items = {};
    }
    set(key, value){ // Dictionary에 값을 추가하는 함수이다.
        this.items[key] = value;
    }

    remove(key){ // Dictionary에서 key 값을 사용하여 value 값을 제거하는 함수이다.
        if (this.haskey(key))
        {
            delete this.items[key];
            return true;
        }
        return false;
    }

    haskey(key2){ // Dictionary에 key가 존재하는지 확인하는 함수이다. 반환값은 불린이다.
        for(var key in this.items)
        {
            if(key == key2)
            {
                return true;
            } 
        }
        return false;
    }

    get(key){ // Dictionary에서 key 값을 사용하여 value 값을 반환하는 함수이다.
        if (this.haskey(key))
        {
            return this.items[key];
        }
        return "No Value";
    }

    keys(){ // Dictionary에 포함된 모든 key 값을 배열로 반환하는 함수이다.
        return Object.keys(this.items);
    }

    values(){ // Dictionary에 포함된 모든 values을 배열로 반환하는 함수이다. 
        var result = []
        for (var key in this.items)
        {
            if (this.haskey(key))
            {
                result.push(this.items[key]);
            }
        }
        return result;
    }

    keyValues(){ // Dictionary에 포함된 모든 key:value 값을 반환하는 함수이다.
        return this.items;
    }

    size(){
        var count = 0;
        for(var key in this.items)
        {
            count++;
        }
        return count;
    }
}


// I-3
var dictionary = new Dictionary
dictionary.set('Gandalf','gandalf@email.com'); 
dictionary.set('John','johnsnow@email.com'); 
dictionary.set('Tyrion','tyrion@email.com'); 
console.log("I-3");
console.log("key 값 존재 확인: ",dictionary.haskey('Gandalf')); // --> key 값 존재 확인:  true
console.log("key 값 존재 확인: ",dictionary.haskey('John')); // --> key 값 존재 확인:  true
console.log("Dictionary 객체의 크기: ",dictionary.size()); // --> Dictionary 객체의 크기:  3
console.log("모든 key 값 출력: ",dictionary.keys()); // --> 모든 key 값 출력:  [ 'Gandalf', 'John', 'tyrion' ]
console.log("모든 values 값 출력: ",dictionary.values()); // --> 모든 values 값 출력:  [ 'gandalf@email.com', 'johnsnow@email.com', 'tyrion@email.com' ]
console.log("key 값에 해당하는 value 출력: ",dictionary.get('Tyrion'),'\n\n'); // --> key 값에 해당하는 value 출력:  tyrion@email.com

// I-4
console.log("I-4");
console.log("key 값에 해당하는 value 삭제: " ,dictionary.remove('John')); // --> key 값에 해당하는 value 삭제:  true  <-- 삭제 성공 시 true, 실패 시 false
console.log("모든 key 값 출력: ",dictionary.keys()); // --> 모든 key 값 출력:  [ 'Gandalf', 'Tyrion' ]
console.log("모든 value 값 출력: ",dictionary.values()); // --> 모든 value 값 출력:  [ 'gandalf@email.com', 'tyrion@email.com' ]
console.log("key 값에 해당하는 value 출력: ",dictionary.get('Tyrion'),"\n\n"); // --> key 값에 해당하는 value 출력:  tyrion@email.com


// I-5
console.log("I-5");
dictionary2 = dictionary.keyValues(); // key:value 값을 요소로 가지는 dictionary 
Object.entries(dictionary2).forEach(([k,v]) => { // 과제에 나와있는 대로 했지만 함수가 아니라는 에러가 나옴 그래서 다음과 같이 수정하니 값이 정상적으로 나옴
    console.log('forEach: ', `key: ${k}   value: ${v}`); 
    // --> forEach:  key: Gandalf   value: gandalf@email.com
    // --> forEach:  key: Tyrion   value: tyrion@email.com
});

// I-6
/* dictionary가 앞으로의 작업에서 중요한 이유 
1. 저장 된 정보의 가독성이 좋다. 
   -> key 값을 통해 value가 어떤 것을 의미하는지 명시적으로 저장할 수 있다.
2. 특정 정보를 찾을 때 key 값을 활용하여 빠르게 찾을 수 있다. 
   -> 정보 관리 시 유용하다
3. 정보의 추가 및 삭제가 빠르게 이루어 진다. 
   -> 인덱스가 이닌 key 값을 사용하여 정보를 저장 하거나 삭제하기 때문에 저장 또는 삭제를 위해 객체를 순차적으로 훑어 위치를 찾을 필요가 없다. 
*/
//================================현우=======================================================