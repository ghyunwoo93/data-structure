var friends = new Array()
var key = ['name','age']
var names = ['jone','Ana','Chris']
var ages = [30,20,25]

for(i=0; i<names.length; i++)
{
    friends[i]={}
    for(j=0; j<key.length; j++)
    {
        if (j==0)
        {
           friends[i][key[j]]=names[i];
        }
        else
        {
            friends[i][key[j]]=ages[i];
        }

    }
}

console.log(friends);


//나이순 정렬 방법 1
//return 값이 -1 이면 a가 b보다 앞, return 값이 1 이면 b가 a보다 앞 . return 값이 0 이면 변화 없음
friends.sort(function compareFriends(a,b) 
{
   if(a.age>b.age)
   {
       return 1;
   }
   else if (a.age<b.age)
   {
       return -1;
   }
   else
   {
       return 0;
   }
}
)

/*나이순 정렬 방법 2
friends.sort(function(a,b)
{
    return a["age"]-b["age"];
}
)
*/

/* 나이순 정렬 방법 3
function compareFriends()
{

    for(i=0; i<friends.length-1; i++)
    {
        for(j=0; j<friends.length-1-i; j++)
        {
            if (friends[j]["age"] >friends[j+1]["age"])
            {
                var temp=friends[j];
                friends[j]=friends[j+1];
                friends[j+1]=temp;
            }
        }
    }
}
compareFriends();
*/

console.log(friends);