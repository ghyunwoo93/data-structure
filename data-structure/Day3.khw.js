// 1-1 Queues 

class Queues
{
    constructor()
    {
        this.datastore=[];
        this.index= 0;
    }
    enqueue(item)
    {
        this.datastore[this.index++]=item;

    }

    dequeue()
    {
        if(this.index==0)
        {
            return null;
        }
        else
        {
            var result = this.datastore[0]
            this.index--
            for (var i=0; i<this.index; i++)
            {
                this.datastore[i]=this.datastore[i+1];
            }
            this.datastore.pop();
            return result;
        }
    }

    peek()
    {
        if (this.index==0)
        {
            return null;
        }
        else
        {
            return this.datastore[0];
        }
    }

    isEmpty()
    {
        if (this.index==0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
/*1.1 Queues 테스트
var queues = new Queues
for (i=0; i<5; i++)
{
    queues.enqueue(i);
}
for (j=0; j<7; j++)
{
    console.log("dequeue() 실행 전 큐 원소: " +queues.datastore);
    console.log("맨 앞 값: " + queues.peek());
    console.log("dequeue()로 인해 빠져 나오는 값: "+queues.dequeue());
    console.log("dequeue() 실행 후 큐 원소: " +queues.datastore);
    console.log("큐가 비였는가? "+ queues.isEmpty());
    console.log("맨 앞 값: " + queues.peek());
    console.log("\n");
}
*/

/* 1-2 Deque
class Deque
{
    constructor()
    {
        this.datastore=[];

    }
    addFront(item)
    {
        this.datastore.unshift(item);
    }

    addBack(item)
    {
        this.datastore.push(item);
    }
    removeFront()
    {
        var result =0;
        if (this.datastore.length==0)
        {
            return null;
        }
        else
        {
            result = this.datastore[0]
            this.datastore.shift();
            return result;
        }
    }
    removeBack()
    {
        var result =0;
        if (this.datastore.length==0)
        {
            return null;
        }
        else
        {
            result = this.datastore[this.datastore.length-1]
            this.datastore.pop();
            return result;
        }
    }
    peekFront()
    {
        if (this.datastore.length==0)
        {
            return null;
        }
        else
        {
            return this.datastore[0];
        }
    }
    peekBack()
    {
        if (this.datastore.length==0)
        {
            return null;
        }
        else
        {
            return this.datastore[this.datastore.length-1];
        }
    }
}
*/
/* 1.2 Deque 테스트
var deque = new Deque
for (i=0; i<5; i++)
{
    deque.addFront(i);
}
console.log(deque.datastore);
for (j=0; j<3; j++)
{
    deque.addBack(j);
}
console.log(deque.datastore);
console.log("뽑힌 Front 값: " + deque.removeFront());
console.log(deque.datastore)
console.log("뽑힌 Back 값: " + deque.removeBack());
console.log(deque.datastore);
console.log("맨 앞 값: ", deque.peekFront());
console.log("맨 뒷 값: ", deque.peekBack());
*/


// 2. circular Queues
function HotPotato(member)
{
    su = member.length;
    while(true)
    {
        var queues = new Queues;
        var num =Math.floor(Math.random()*10+5); // 5~14
        //console.log(num);
        var count =1;
        for (i=0; i<num; i++)
        {
            queues.enqueue(member[i%su])
            count ++;
            if (count>su)
            {
                var out = queues.dequeue();
            }
        }
        console.log (out+" was eliminated from the Hot Potato game\n");
        member = queues.datastore;
        //console.log(member);
        --su ;
        //console.log(su);
        if (su==1)
        {
            console.log("The winnder is: "+queues.datastore[0]);
            break;
        }
    }

}
var member = ["John","Jack","Camila","lngrid","Carl"];
HotPotato(member);
