
interface Person {
    name: string;
    age: number;
}

let friends: Array<Person> = 
[
    {name: 'john', age: 30},
    {name: 'Ana', age: 20},
    {name: 'Chris', age: 25}
];

function compareFriends()
{
    friends.sort((a:Person, b:Person): number => {
        return a.age - b.age;
    });
    console.log(friends)
}

compareFriends();
