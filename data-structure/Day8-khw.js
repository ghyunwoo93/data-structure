// I.Separate Chaining

class ValuePair { // 해시 테이블에 key:value 값으로 저장하기 위한 클래스
    constructor(key, value) {
    this.key = key;
    this.value = value;
    }
    toString() {  // 출력을 위한 toString 함수
        return `[#${this.key}: ${this.value}]`;
    }
}
    
class HashTableSeparateChaining { // 분리 연결법을 위한 클래스
    constructor() {
    this.table = {}; // 딕셔너리 형태의 해시 테이블 선언 및 초기화
    }
    loseloseHashCode(key) { // 해시 값을 만드는 함수
        if (typeof key === 'number') { // key 타입이 숫자일 경우 해시 값으로 변경 없이 그냥 사용
            return key;
        }
        const tableKey = key.toString(); // key 타입이 숫자가 아닐경우 문자로 변경 -> charCodeAt을 사용하기 위함 (문자를 유니코드로 변경시킨다.)
        let hash = 0; // 해시 값 담을 변수를 선언 및 초기화
        for (let i = 0; i < tableKey.length; i++) { // 해시 값을 만드는 과정 1. 문자의 한 글자마다 유니코드로 변경 후 더함
            hash += tableKey.charCodeAt(i); 
        }
            return hash % 37; // 해시 값을 만드는 과정 2. 위에서 더한 값을 37로 나눈 나머지가 최종 해시값이다.
    }
        
    hashCode(key) { //해시 코드를 반환 해주는 함수
            return this.loseloseHashCode(key);  
    }
    
    put(key, value) { // key 및 value를 해시테이블에 넣는 함수이다.
        if (key != null && value != null) { // key, value 모두가 존재해야 한다.
            const position = this.hashCode(key); // key 값을 이용하여 해시 값을 구한다.
            if (!this.table[position]) // 해시 테이블에 해당 해시 값이 비어 있다면 배열을 만든다. (같은 해시값인데, key와 value가 다를 경우 그 값을 넣기 위함)
            {
                this.table[position] = [];
            }
            this.table[position].push(new ValuePair(key, value)); // 해시 테이블에 해당 해시 주소에 key:value로 저장된 값들을 넣는다 (클래스 값)
            return true;
        }
            return false;
    }

    Search(key){ // 해시 테이블에서 key 값의 위치를 찾는 함수이다.
        const valuePair = this.table[this.hashCode(key)]; // 해시 테이블에서 해당 키의 주소 값을 찾아서 저장한다.
        if (valuePair == null) { // 주소값이 비였을 경우 undefined를 반환한다.
            return undefined;
        }
        else
        {
            for (let i=0; i<valuePair.length; i++) 
            {
                if (valuePair[i].key==key) // 해당 주소값에 있는 key:value 값들의 key 값과 입력 받은 key 값이 같은지 비교하고 맞으면 그 위치를 반환한다.
                {
                        return i;
                }
            }
            return undefined; // 해당 주소값에 입력 받은 key 값이 없ㅁ으면 undefined를 반환한다.
        }
    }

    get(key) { // 해시 테이블의 해당 주소값에서 입력 key 값에 해당하는 해시 값, key 값, value 값을 반환한다.
        const hash = this.hashCode(key); // 해시 값을 만든다.
        var Linkindex = this.Search(key); // 해시 값에서 해당 key 값에 해당하는 위치를 찾는다.
        if(Linkindex== undefined){ // 없으면 undefined 를 반환한다.
            return Linkindex;
        }
        else{ // 있으면, 해당 주소값에서 입력 key 값에 해당하는 해시 값, key 값, value 값을 반환한다.
            return "hash값: "+hash+"\t\tkey값: "+this.table[hash][Linkindex].key+"\t\tvalue값: "+this.table[hash][Linkindex].value;
        }
    }
    
    remove(key) { // 해시 테이블의 해당 주소값에서 입력 key 값에 해당하는 key 값, value 값을 제거한다.
        const hash = this.hashCode(key);  // 해시 값을 만든다.
        var Linkindex = this.Search(key)  // 해시 값에서 해당 key 값에 해당하는 위치를 찾는다.
        console.log("제거 대상 ",this.get(key)); // 제거 할 값을 출력한다.
        if(Linkindex==undefined){// 없으면 undefined 를 반환한다.
            return false;
        }
        else
        {
            this.table[hash].splice(Linkindex,1); // 해당 key, value를 제거한다.
            if(this.table[hash].length==0){ // 제거 후, 해당 해시 주소값에 요소가 아무것도 없으면 해시 주소값을 제거한다.
                delete this.table[hash];
            }
            return true;
        }
    }

    toString() {
        if (Object.keys(this.table).length == 0) { // key 값(해시 주소 값)이 없을 때 반환하는 부분
            return '';
        }
        const keys = Object.keys(this.table); // key 값(해시 주소 값)을 구한다.
        let objString = `{${keys[0]} => ${this.table[keys[0]].toString()}}`; // 변수 선언 및 첫번 째 주소 값과 그에 해당하는 key, value 값을 출력하는 문장으로 초기화한다.
        for (let i = 1; i < keys.length; i++) {
                objString = `${objString},{${keys[i]} => ${this.table[keys[i]].toString()}}`; // 해시 테이블의 주소 값들과 그에 해당하는 key, value 값들을 출력하는 문장을 계속 쌓는 부분이다.
        }
        return objString; // 해시 테이블의 모든 주소값 및 각각에 해당하는 key,value를 문장 형태로 담고 있는 변수를 반환한다.
    }

}
// 4-1 Class HashTableSeparateChaining, put(), toString(), conosle.log()를 이용하여 다음의 값이 출력되도록 코드를 작성
const hash = new HashTableSeparateChaining;
console.log("4-1")
hash.put('Ygritte', 'ygritte@email.com');
hash.put('Jonathan', 'jonathan@email.com');
hash.put('Jamie', 'jamie@email.com');
hash.put('Jack', 'jack@email.com');
hash.put('Jasmine', 'jasmine@email.com');
hash.put('Jake', 'jake@email.com');
hash.put('Nathan', 'nathan@email.com');
hash.put('Athelstan', 'athelstan@email.com');
hash.put('Sue', 'sue@email.com');
hash.put('Aethelwulf', 'aethelwulf@email.com');
hash.put('Sargeras', 'sargeras@email.com');
console.log(hash.toString(),"\n\n");
/* 출력문
4-1
{4 => [#Ygritte: ygritte@email.com]},{5 => [#Jonathan: jonathan@email.com],[#Jamie: jamie@email.com],[#Sue: sue@email.com],[#Aethelwulf: aethelwulf@email.com]},
{7 => [#Jack: jack@email.com],[#Athelstan: athelstan@email.com]},{8 => [#Jasmine: jasmine@email.com]},{9 => [#Jake: jake@email.com]},
{10 => [#Nathan: nathan@email.com],[#Sargeras: sargeras@email.com]} 
*/


// 4-2 remove(), get(), toString(), console.log()를 이용하여 다음의 값이 출력되도록 코드를 작성하세요.
console.log("4-2")
console.log("제거 성공 여부(true/false):",hash.remove('Ygritte'));
console.log(hash.toString(),"\n\n");
/* 출력문
4-2
제거 대상  hash값: 4		key값: Ygritte		value값: ygritte@email.com
제거 성공 여부(true/false): true
{5 => [#Jonathan: jonathan@email.com],[#Jamie: jamie@email.com],[#Sue: sue@email.com],[#Aethelwulf: aethelwulf@email.com]},
{7 => [#Jack: jack@email.com],[#Athelstan: athelstan@email.com]},{8 => [#Jasmine: jasmine@email.com]},{9 => [#Jake: jake@email.com]},
{10 => [#Nathan: nathan@email.com],[#Sargeras: sargeras@email.com]} 
*/


// 4-3 remove(), get(), toString(), console.log()를 이용하여 다음의 값이 출력되도록 코드를 작성하세요
console.log("4-3")
console.log("제거 성공 여부(true/false):",hash.remove('Jamie'));
console.log("제거 성공 여부(true/false):",hash.remove('Sue'));
console.log("제거 성공 여부(true/false):",hash.remove('Aethelwulf'));
console.log(hash.toString());
/* 출력문
4-3
제거 대상  hash값: 5		key값: Jamie		value값: jamie@email.com
제거 성공 여부(true/false): true
제거 대상  hash값: 5		key값: Sue		value값: sue@email.com
제거 성공 여부(true/false): true
제거 대상  hash값: 5		key값: Aethelwulf		value값: aethelwulf@email.com
제거 성공 여부(true/false): true
{5 => [#Jonathan: jonathan@email.com]},{7 => [#Jack: jack@email.com],[#Athelstan: athelstan@email.com]},{8 => [#Jasmine: jasmine@email.com]},
{9 => [#Jake: jake@email.com]},{10 => [#Nathan: nathan@email.com],[#Sargeras: sargeras@email.com]}
*/


//II Linear Probing
class ValuePair { // 해시 테이블에 key:value 값으로 저장하기 위한 클래스
    constructor(key, value) {
    this.key = key;
    this.value = value;
    }
    toString() {  // 출력을 위한 toString 함수
        return `[#${this.key}: ${this.value}]`;
    }
}

class HashTableLinearProbing { // 해시 테이블 선형 체이닝 클래스
    constructor() {
    this.table = {}; // 딕셔너리 형태의 해시 테이블 선언 및 초기화
    }
    
    loseloseHashCode(key) { // 해시 값을 만드는 함수
        if (typeof key === 'number') { // key 타입이 숫자일 경우 해시 값으로 변경 없이 그냥 사용한다.
            return key;
        }
        const tableKey = key.toString(); // key 타입이 숫자가 아닐경우 문자로 변경한다. -> 문자를 유니코드로 변경시키는 charCodeAt을 사용하기 위함이다.
        let hash = 0; // 해시 값을 담을 변수 선언 및 초기화
        for (let i = 0; i < tableKey.length; i++) { // 해시 값을 만드는 과정 1. 문자의 한 글자마다 유니코드로 변경 한 후 더한다.
            hash += tableKey.charCodeAt(i); 
        }
        return hash % 37; // 해시 값을 만드는 과정 2. 위에서 더한 값을 37로 나눈 나머지가 최종 해시값 된다.
    }
            
    hashCode(key) { //해시 코드를 반환 해주는 함수이다.
            return this.loseloseHashCode(key);  
    }
    
    put(key, value) { // 해시 테이블에 해당 해시 주소값에 key, value를 저장하는 함수
        if (key != null && value != null) { // key 값과 value 값이 모두 있을 시, 수행한다.
            const position = this.hashCode(key); // 해시 주소 값을 저장한다.
            if (this.table[position] == null) { // 해시 테이블에 해당 해시 주소값에 값이 없을 때,
                this.table[position] = new ValuePair(key, value); // key, value 값을 저장한다.
            } 
            else { // 해시 테이블에 해당 해시 주소값에 값이 있을 때,
                let index = position + 1; // 해시 주소 값을 1 증가 시킨다.
                while (this.table[index] != null) { // 해시 테이블에 값이 없는 주소 값을 찾을 때 까지 반복한다.
                    index++; // 해시 주소 값 1씩 증가
                }
                this.table[index] = new ValuePair(key, value); // 값이 없는 주소 값을 찾으면 key, value 값을 저장한다.
            }
            return true;
        }
        return false;
    }
                
                
    get(key) { // 해시 테이블에 해당 해시 주소 값의 value값을 출력하는 함수.
        const position = this.hashCode(key); // 해시 주소 값을 저장한다.
        if (this.table[position] != null) { // 해시 주소 값에 값이 있을 경우 수행한다.
            if (this.table[position].key === key) { // 해시 테이블에서 해당 주소값에 저장된 key 값과 입력된 key 값을 비교해서 같을 때,
                return this.table[position].value; // 해당 주소 값에 저장된 value 값을 반환한다.
            }
            let index = position + 1; // 같지 않을 때, 해시 주소 값을 증가 시킨다. (밀려서 저장되어 있을 수 있으므로)
            while (this.table[index] != null && this.table[index].key !== key) { // 테이블의 값이 비어 있거나 입력 키 값과 해시 주소 값 내의 key 값이 같을때 까지 반복한다.
                index++; // 해시 주소 값 1씩 증가
            }
            if (this.table[index] != null && this.table[index].key === key) { // 테이블의 값이 비어 있지 않고, 입력 키 값과 해시 주소 값 내의 key 값이 같다면 그때의 해시 주소 값의 value를 반환한다. 
                return this.table[position].value; 
            }
        }
        return undefined; // 해시 주소 값에 값이 없으면 undefined를 반환한다.
    }

    remove(key) { // 해시 테이블에서 입력 key 값에 해당하는 해시 주소 값을 삭제한다.
        const position = this.hashCode(key); // 해시 주소 값 저장한다.
        if (this.table[position] != null) { // 해시 주소 값에 값이 있을 경우 수행한다.
            if (this.table[position].key === key) { // 해당 주소 값의 key 값과 입력한 key 값이 일치하면 실행.
                delete this.table[position]; // 해당 주소 값을 삭제한다.
                this.verifyRemoveSideEffect(key, position); // key 값과 삭제된 주소 값을 다음 함수의 파라미터로 보냄과 동시에 호출한다.
                return true;
            }
            let index = position + 1; // 해당 주소 값의 key 값과 입력과 key 값이 일치하지 않으면 해시 주소 값을 1 증가시킨다.
            while (this.table[index] != null && this.table[index].key !== key ) {  // 테이블의 값이 비어 있거나 입력 키 값과 해시 주소 값 내의 key 값이 같을때 까지 반복한다.
                index++; // 해시 주소 값 1씩 증가
            }
            if (this.table[index] != null && this.table[index].key === key) { // 테이블의 값이 비어 있지 않고, 입력 키 값과 해시 주소 값 내의 key 값이 같을 경우 실행한다.
                delete this.table[index]; //  해당 해시 주소 값을 삭제한다.
                this.verifyRemoveSideEffect(key, index); // key 값과 삭제된 주소 값을 다음 함수의 파라미터로 보냄과 동시에 호출한다.
                return true;
            }
        }
        return false;
    }
                        
    verifyRemoveSideEffect(key, removedPosition) { // 해시 테이블에서 해시 주소값이 삭제될 경우, 뒤에 밀린 값을 삭제된 해시 주소 값에 넣는 함수이다.
        const hash = this.hashCode(key); // 삭제된 key 값의 hash 값을 구한다. 
        let index = removedPosition + 1; // 삭제 된 해시 주소 값에 1을 더해서 저장한다. -> 삭제 된 주소 값 뒤에 값을 넣는 것이므로 바로 뒤부터 시작이기 때문이다.
        while (this.table[index] != null) { // 해시 테이블의 해시 주소 값에 값이 없을 때 까지 반복한다. -> 없다는 것은 밀린 것이 없다는 뜻이다.
            const posHash = this.hashCode(this.table[index].key); // 다음 위치의 key값의 해시 값을 구해서 저장한다.
            if (posHash <= hash || posHash <= removedPosition) { // 제거된 해시 주소 값 보다 다음 위치의 해시 값이 작거나, 제거된 위치의 해시 값의 주소보다 다음 위치의 해시 값이 작으면 수행한다. 
                this.table[removedPosition] = this.table[index]; // 제거된 해시 주소 값에 조건을 만족하는 해시 주소 값의 key,value 값을 넣는다.
                delete this.table[index]; // 원래 위치의 해시 주소 값의 key,value를 제거한다.
                removedPosition = index; // 제거된 해시 주고 값을 방금 제거한 해시 주소 값의 위치로 변경한다.
            }
            index++; // 해시 주소 값을 증가시키면서 수행한다.
        }
    }
    toString() {
        if (Object.keys(this.table).length == 0) { // key 값(해시 주소 값)이 없을 때 반환하는 부분
            return '';
        }
        const keys = Object.keys(this.table); // key 값(해시 주소 값)을 구한다.
        let objString = `{${keys[0]} => ${this.table[keys[0]].toString()}}`; // 변수 선언 및 첫번 째 주소 값과 그에 해당하는 key, value 값을 출력하는 문장으로 초기화한다.
        for (let i = 1; i < keys.length; i++) {
                objString = `${objString},{${keys[i]} => ${this.table[keys[i]].toString()}}`; // 해시 테이블의 주소 값들과 그에 해당하는 key, value 값들을 출력하는 문장을 계속 쌓는 부분이다.
        }
            return objString; // 해시 테이블의 모든 주소값 및 각각에 해당하는 key,value를 문장 형태로 담고 있는 변수를 반환한다.
    }    
}

// 5=1 class HashTableLinearProbing, put(), toString(), console.log()를 이용하여 다음과 같이 출력되도록 코드를 작성하세요.
const hash = new HashTableLinearProbing;
console.log("5-1")
hash.put('Ygritte', 'ygritte@email.com');  //4
hash.put('Jonathan', 'jonathan@email.com'); // 5
hash.put('Jamie', 'jamie@email.com'); //6  - 5
hash.put('Jack', 'jack@email.com'); // 7 
hash.put('Jasmine', 'jasmine@email.com'); //8
hash.put('Jake', 'jake@email.com'); // 9
hash.put('Nathan', 'nathan@email.com'); //10
hash.put('Athelstan', 'athelstan@email.com'); //11 -7
hash.put('Sue', 'sue@email.com'); // 12 -5 
hash.put('Aethelwulf', 'aethelwulf@email.com'); //13 - 5
hash.put('Sargeras', 'sargeras@email.com'); // 14 - 10
console.log(hash.toString(),"\n\n");
/* 출력문
5-1
{4 => [#Ygritte: ygritte@email.com]},{5 => [#Jonathan: jonathan@email.com]},{6 => [#Jamie: jamie@email.com]},{7 => [#Jack: jack@email.com]},
{8 => [#Jasmine: jasmine@email.com]},{9 => [#Jake: jake@email.com]},{10 => [#Nathan: nathan@email.com]},{11 => [#Athelstan: athelstan@email.com]},
{12 => [#Sue: sue@email.com]},{13 => [#Aethelwulf: aethelwulf@email.com]},{14 => [#Sargeras: sargeras@email.com]} 
*/

// 5-2 remove(), get(), toString(), console.log()를 이용하여 다음과 같이 출력되도록 코드를 작성하세요.
console.log("5-2")
hash.remove('Ygritte');
hash.remove('Jonathan')
console.log(hash.toString());
/* 출력문
5-2
{5 => [#Jamie: jamie@email.com]},{6 => [#Sue: sue@email.com]},{7 => [#Jack: jack@email.com]},{8 => [#Jasmine: jasmine@email.com]},
{9 => [#Jake: jake@email.com]},{10 => [#Nathan: nathan@email.com]},{11 => [#Athelstan: athelstan@email.com]},
{12 => [#Aethelwulf: aethelwulf@email.com]},{13 => [#Sargeras: sargeras@email.com]} 
*/

/* 3. Separate chaining과 Linear probing의 차이
Separate chaining
분리 연결법은 해쉬 테이블의 버킷에 하나의 값이 아니라 링크드 리스트(Linked List)나 트리(Tree)를 사용한다

Linear probing 
선형 탐사법(Linear Probing)은 말 그대로 선형으로 순차적으로 탐사하는 방법으로 충돌이 났을때 정해진 n칸만큼의 옆 방을 홀드해서 데이터를 저장한다.
*/


