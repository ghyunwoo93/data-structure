/*
    Stack?
    - 먼저 들어간 자료가 나중에 나오는 후입 선출 자료 구조로, LIFO(Last In First OUR)이라고 부른다.
    - 메서드
        - push() : 스택에 요소를 저장한다.
        - pop()  : 스택에서 요소를 꺼낸다.
        - peek() : 스택의 최상위 요소를 반환한다.
        - clear(): 스택을 초기화 한다.
        - isEmpty(): 현재 스택이 비어있는지를 확인한다.
        - size(): 해당 스택의 max size를 반환한다.
*/
class Stack{
    constructor() {
        this.datastore = []
        this.index = 0;
    }
    push(item)
    {
        this.datastore[this.index++] = item ;
    }
    pop() 
    {
        if (this.index ==0) 
        {
            return null;
        }
        else
        {
            var result = this.datastore[--this.index]
            this.datastore.splice(this.index,1);
            return result;
        }
    }
    peek()
    {
        if (this.index == 0)
        {
            return null;
        }
        else
        {
            var result = this.datastore[this.index-1]
            return result;
        }
    }
    isEmpty()
    {
        if (this.datastore.length==0)
        {
            console.log("스택이 비었습니다.")
        }
        else
        {
           console.log(this.datastore); 
        }
    }
    clear()
    {
        this.datastore=[];
        console.log(this.datastore);
    }
    size()
    {
        var result = this.datastore.length;
        return result;
    }

}

/* 기능 확인
// 스택 생성
var stack = new Stack

// push, size 기능 확인
for (i=1; i<20; i+=2)
{
    stack.push(i);
    console.log("size: "+stack.size());
}
console.log()

// clear, isEmpty, size 기능 확인  
stack.clear();
console.log(stack.size());
stack.isEmpty();

*/

/* 기능 확인
// 스택 생성 
var stack = new Stack

// push , size 기능 확인
for (i=1; i<20; i+=2)
{
    stack.push(i);
    console.log("size: "+stack.size());
}
console.log()

// pop 및 isEmpty, peek 기능 확인 
for (i=1; i<20; i++)
{
    console.log("pop: "+stack.pop());
    stack.isEmpty();
    console.log("peek: "+stack.peek());
}
*/


function decimalToBinary(n)
{   
    var first = n;
    var stack = new Stack();
    while (n>0)
    {
        mod = n%2;;
        n = Math.floor(n/2);
        stack.push(mod);
    }
    var result = "";
    while(stack.size()>0)
    {
        result+=String(stack.pop());
    }
    console.log('10진수 : '+ first +'\t->\t2진수 : '+ result);
}

decimalToBinary(233);
decimalToBinary(10);
decimalToBinary(1000);

