var friends = [
    { name: 'john', age: 30 },
    { name: 'Ana', age: 20 },
    { name: 'Chris', age: 25 }
];
function compareFriends() {
    friends.sort(function (a, b) {
        return a.age - b.age;
    });
    console.log(friends);
}
compareFriends();
